#!/bin/bash

IMGPATH=$1
QUALITY=75
MAXSIZE=1500

echo Optimizing images ...

for file in $(find $IMGPATH -type f -name "*.jpg");
do
    echo Optimizing $file, Quality $QUALITY ...
    convert -strip -interlace Plane -resize $MAXSIZEx$MAXSIZE\> -quality $QUALITY $file $file
done

# for file in $(find $IMGPATH -type f -name "*.png");
# do
#     echo Optimizing $file ...
#     convert -strip -resize $MAXSIZEx$MAXSIZE\> $file $file
#     optipng -o7 $file
# done
