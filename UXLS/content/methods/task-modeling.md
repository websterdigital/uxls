+++
date = "2018-01-08T00:00:00+01:00"
featured = false
title = "Task Modeling"
short_title = "Task Modeling"
weight = "100"

[header]
  thumbnail = "task-model.jpg"
  image = "task-model.jpg"
  summary = "Understand what tasks your users are trying to accomplish before designing solutions to treat pain points areas."
  
[menucontent]
  section = "user-research"


  [menu.main]
    parent = "methods"

[sidebar]
  project_stage = "User Research"
  difficulty = "Medium"
  great_for = ["Understanding the tasks to be supported by a new system", "Focusing on the goals the user has rather than recommended design solutions	"]
  materials = ["User research such as interviews or contextual inquiry", "Software to capture the output of your task model (e.g. text based editor)"]
  resources = [""]
  takes = "2-3 weeks"

[capabilities]
  user_reqs = "yes"
  focus = "yes"
  evaluate = "no"
  generate = "no"
  measure_ux = "no"
  compare = "no"
  
+++

## Overview

The Task Model allows you to understand what tasks your users are performing in order to achieve their desired goals. It is a method that allows you to dissect user’s tasks in greater detail enabling you to identify all the required steps for a specific task, the order of those steps, and combined with user research, the reason why people are performing those tasks. It should always be performed after some initial user research.

You might want to conduct Task Modelling when:

- You have already conducted some user research but need to understand the process and tasks involved for each user group goal.
- You have an existing system and want to identify improvements.

### Who’s involved?

- UX specialist
- Users
- Stakeholders or project members with knowledge of the tasks


## How To

### I. Identify user groups and goals 

1. **Choose area of focus**\
To perform task modelling, start by determining which user groups to focus on and their respective goals.
2. **Identify core tasks**\
For each goal, use previous research from these user groups to identify the core tasks to achieving those goals and why your users perform those core tasks. If the core tasks are unclear, do some more user research before going onto the next step.

#### Tips:

- Using the methods Personas and Jobs-to-be-done are a good way of identifying user groups and goals.
- Don't worry too much if every step is not detailed. Provided there is some understanding of the core tasks, iterative refining is best.

### II. Create the Task Model

Each core task is always linked to a specific user group. The same core task could be done by different user groups but it is important when developing your task model to have one specific user group in mind for each core task.

The task model for each core task consists of:

- A concise description of the task
- Precondition -- What triggers the task
- Subtasks -- What steps are necessary to process for reaching the intended goal
- Postcondition -- Intended outcome


1. **Consider the general process model for conducting tasks**\
To capture all of the subtasks it is helpful to look at the following general process model of conducting tasks that has been derived from occupational psychology.\\
For each of the core tasks, consider all the steps in the general process model ensuring the capture of all sub-tasks. This includes actions and decisions that the user has to perform in order to achieve the desired result.\\
**Plan → Prepare → Execute → Assess result → Deliver result**
2. **Use the table below as an example of Task Modeling**\
The table below details an example of a task model for cleaning windows.

|Part of task model  | Example           |
|-------------------------|------------------------------------|
|Task description (should be concise and at least consist of a verb and a noun)                |Cleaning windows|
|Precondition (what triggers the task)              |Dirty windows|
|- Plan                                             |Determine when to clean the windows|
|- Prepare                                          | -Fill water into bucket  - Add cleaning agent - Get sponge and cloth - Clear the windowsill|
|- Execute                                          |- Wash window using the sponge - Dry window with the cloth|
|- Assess result                                       |Check if window is clean and no stripes are visible|
|- Deliver result                                         |(Not applicable for this task)|
|Intended outcome (Post-condition) | Clean windows |
Table 1: The table above is a simple example of part of a task model illustrating the task of cleaning windows including a brief description of the task and an intended outcome.


### III. Refining the Task Model

1. **Make a list of any questions and knowledge gaps**\
In thinking of the task model, it’s likely that the activity would have prompted a number of questions in which there are knowledge “gaps”. Noting these will help to refine the model. 
2. **Identify user research methods for each question**\
Methods may include interviews, contextual inquiry, or diary studies.
3. **Group questions into categories**\
Once questions are grouped into relevant research methods, identify how many users are to be involved.
4. **Perform directed User Research**\
Use findings to iterate task model to reflect any changes.

## Tips for Life Sciences

- Collaborate with stakeholders, users, and project members in the life sciences through workshops to leverage shared knowledge to complete a task model.
- In complex tasks, stakeholders and users might have an idealised view of the process and how they perform their task. Observation or diary studies are a good way to validate the task model with real users and in their context.
- Keep the focus of any conversations with end users and stakeholders on problems they face, rather than possible solutions.
- Find out "why" people are performing these steps in the task. This will reveal gaps and open questions to be asked in following user research.
- Stakeholders and project members might be biased by solutions. Ensure your task model is validated with actual end users; such as bench scientists or lab coordinators.

## Resources

- [Task Model Template (docx)](Task-Model-Template.docx)