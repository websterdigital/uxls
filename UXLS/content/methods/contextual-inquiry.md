+++
date = "2017-06-10T21:32:03+01:00"
featured = false
title = "Contextual Inquiry"
short_title = "Contextual Inquiry"
weight = "100"
related_case_studies = ["contextual-inquiry.md"]

[header]
  thumbnail = "contextual_inquiry.jpg"
  image = "contextual_inquiry.jpg"
  summary = "By directly observing users and asking the right questions, you can see for yourself how they work and better understand needs and opportunities."

[menucontent]
  section = "user-research"

  [menu.main]
    parent = "methods"

[sidebar]
  project_stage = "User Research"
  difficulty = "Low"
  great_for = ["Understanding where and how users actually work", "Uncovering workarounds, references, and shortcuts"]
  materials = ["Facilitator Guide – Contextual Inquiry", "Note-taking materials", "Audio or video recorder (optional)"]
  takes = "1-2 weeks"

[capabilities]
  user_reqs = "yes"
  focus = "yes"
  evaluate = "no"
  generate = "no"
  measure_ux = "no"
  compare = "no"
+++

## Overview

Contextual Inquiry involves interviewing and observing someone as they work in their own environment.

By observing and asking probing questions, you can see what users do, better understand why they work in a certain way, and uncover otherwise "hidden" insights that users may not naturally think to share.

### Who's involved?

- Facilitator
- Participant
- Optional: 1--2 note-takers/observers (e.g. a project team member or key stakeholder)

## How To

### I. Plan and Prepare

1. **Identify and prioritize goals**\
Understand what aspects of the work and environment you want to evaluate.
2. **List the specific work activities to observe and associated questions**\
Note anything you are interested in discovering about the work context, such as interruptions, noise level, clutter, etc.
3. **Identify and select participants**\
Participants should represent a variety of dimensions (e.g. length of time in role, location) based on the goals.
4. **Determine logistics and schedule**\
Plan to conduct the sessions where the participants naturally work.

#### Tips:

- Be considerate of others working in the same environment.
- Select participants who are comfortable being observed and talking about their work.
- You might need to do some homework to familiarize yourself with a basic understanding of the environment or work.
- If you plan to record the session or take photos, be sure to follow any policies, rules or practices around informed consent.

### II. Run the session

1. **Meet the participant where they work and provide an overview of the session**\
Include details on participant consent and the goals of the session. (Optional: If you plan to record the session and have participant consent, begin recording.)
2. **Start the session by asking participants general questions about their role, their work and their work environment**\
This sets the stage for observing their work.
3. **Have the participant perform their work as they naturally would**\
Note any important or interesting observations, behaviors, flow of work, workarounds, reference materials, etc.  Ask questions as needed to understand what is happening and why.
4. **After their work is complete, ask probing questions on specific observations**\
Go deeper on the important, unexpected, or interesting things you observed to more fully understand their work in context.
5. **Close the session**\
Provide any closing remarks and thank the participant.

#### Tips:

- Let the participant work as they normally would and avoid directing them to carry out tasks differently.
- Pay attention to interruptions or distractions to the flow of work, and how the participant gets back on task.
- Don’t forget to observe the physical space, informal shortcuts and work-arounds, and social environment as well. Use the Observation Template to take notes.
- Ask the participant to “show me” to help them articulate their work practices.

### III. Analyze and Report
1. **Compile and review all notes, photos, and recordings**
2. **Analyze, summarize and categorize key observations, trends, patterns, and comments that align to the original goals**\
Note anything unexpected but insightful.
3. **Document the set of prioritized findings and recommendations**\
In general, findings are facts supported by data; recommendations are suggestions for improvement.
4. **Share the report with developers, project team members, or other key stakeholders**\
5. **Work with the project team to determine any next steps in response to the report**\

#### Tips:
- Based on the audience for the report, consider supporting findings with details on frequency, impact, severity, etc.
- Take advantage of visual artifacts gathered during the sessions (photos, drawings or videos) to illustrate specific findings and observations.


## Tips for Life Sciences

- You may need to prepare on the subject matter or work process in advance to better understand what you will be observing and ask relevant probing questions
- Consider the complexity of the work being performed – it may not have a linear flow
- If in a lab, consider any general environmental/safety implications (e.g. personal protective equipment)
- In a lab environment make sure you are briefed on any specific dangerous substances or microorganisms, etc. that will be in use, and how you can observe but not get in people's way

## Resources

- [Observations Template (docx)](observations_template.docx) - Use this template to note key observations, comments or other relevant details surfaced during a contextual inquiry.

