+++
date = "2018-01-08T00:00:00+01:00"
featured = false
title = "Card Sorting"
short_title = "Card Sorting"
weight = "200"

[header]
  thumbnail = "card-sort.jpg"
  image = "card-sort.jpg"
  summary = "Direct feedback from the users can help to better prioritize content, design screen layouts, and understand the users’ perspectives."

[menucontent]
  section = "ui-design"

  [menu.main]
    parent = "methods"

[sidebar]
  project_stage = "UI Design"
  difficulty = "Medium"
  great_for = ["Understanding users’ mental models, organizational relationships, and collective vocabulary", "Determine content priority", "Developing site maps, navigational hierarchies, form organization, screen layout, task processes, and other outputs where an understanding of the users’ perspectives of concept relationships is important."]
  materials = ["Session Facilitation Guide", "Index cards and Pens", "Camera", "Audio and/or video recorder (optional)"]
  resources = [""]
  takes = "1-3 weeks"

[capabilities]
  user_reqs = "yes"
  focus = "no"
  evaluate = "no"
  generate = "yes"
  measure_ux = "no"
  compare = "no"
  
+++

## Overview

A card sort is an exercise in which participants categorize concepts into different groups based on their own understanding of those concepts.

You will need to choose from two types of card sort:

- **Open Card Sort** -- Participants organize concepts into groupings of their own creation.  Use an open card sort to understand how concepts fit into participants’ categorical structure and terminology.
- **Closed Card Sort** -- Participants sort concepts into predefined categories selected by the team. Use a closed card sort to understand how concepts fit within a pre-defined set of categories, for example, a pre-existing navigational structure.

### Who’s involved?

- Session facilitator
- Participant(s)
- Optional: 1--2 note-takers/observers (e.g. a project team member or key stakeholder)

## How To

### I. Plan and Prepare

1. **Identify goals and decide type of card sort to use**\
Understand what insight you hope to find by running the card sort and decide which type best matches your situation.
2. **List the specific concepts to organize**\
Make a prioritized list of all the concepts that you wish to understand the relationship between.
3. **Select and schedule participants**\
Participants should represent a variety of dimensions (e.g. length of time in role, location, and function) based on the goals. Plan to conduct the sessions where the participant has a large surface to spread out the concepts to organize.
4. **Develop Session Facilitation Guide**\
This guide helps you run the session with an at-a-glance view of instructions and discussion points.

#### Tips:

- Limit the number of cards. It is tempting to want to sort all of your concepts, but be mindful of participant fatigue. Use a maximum of 30 concepts. If you have more, plan multiple rounds of card sort to gain a full picture of the users’ mental model.
- Select participants who are comfortable being observed and talking about their organizational choices.
- Leave breaks between research sessions to allow for any overrun, give time to capture the organizational scheme developed, reset the test for the next person, and to have a break.
- Be aware of any company policies, rules, practices, or relevant regulations around incentives (if using), audio/video recording, and consent for participation.

### II. Run the Session

1. **Greet the participant and provide an overview of the session**\
Inform participants that you are not “testing” them, and that any input will be kept anonymous.
2. **Provide instructions and allow the participant to proceed**\
Ask participants to separate the cards into categories that make sense to them. For closed card sort, ask them to use your predefined categories. For open card sort, ask them to label the groupings.
3. **Have participants explain their groupings**\
Ask the participant to explain how they developed their card organization. This can provide insights that are more useful than the groupings themselves.
4. **Close the session and thank the participant**\
Provide any closing remarks and give the participant your contact details, in case they have questions later.
5. **Capture the session outputs**\
Take notes and/or photos of the output groupings and terms. Take special note if a participant believed a certain card should not be in the mix, or if a category is misnamed.

#### Tips:

- Note any concepts a participant struggled to sort.
- Avoid interrupting. Silence can be useful by giving the participant space to think and respond thoughtfully.
- Randomly shuffle the cards between sessions to avoid a presentation order bias.
- Numbering the cards may make recording them after a session easier, as you can write the card number rather than the concept text.

### III. Analyze and Report

1. **Analyze recorded data**\
Analyze qualitative information based on the user's feedback and thoughts.
Analyze quantitative information based on the frequency of cards that were grouped together in specific categories.
2. **Generate a report based on the analysis**\
Your report should include key findings, pain point areas, challenges, information the participant liked to look at, and any recommendations for improvements.

#### Tips:

- Make recommendations for improving the information architecture.
- If you are completing a less detailed analysis, find commonalities in how participants group concepts in multiple rounds of sorting.
- To analyze the quantitative relationship between the cards and categories, use spreadsheet features, such as pivot tables.

## Tips for Life Sciences

- Scientists often deal with large sets of data that can be organized in multiple ways. Use card sorting to find the most relevant facets for browsing and searching. 
- Scientists will organize concepts using their specific background, research focus, and scientific discipline. This can result in different categorizations of concepts. To resolve this, run a card sort with scientists working in pairs or small groups. The scientists can negotiate a categorization that balances the needs of their different perspectives
- It may be useful to explore edge cases and exceptions that are difficult to categorize.  Embracing the complexity can help you to understand the organizational challenge.
- Scientists will want to add new cards and categories based on their experiences, so plan accordingly. 
