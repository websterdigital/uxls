+++
date = "2017-06-15T06:22:09+01:00"
featured = false
title = "System Usability Scale"
short_title = "SUS"
weight = "100"
related_case_studies = ["sus.md"]

[header]
  thumbnail = "sus.jpg"
  image = "sus.jpg"
  summary = "Use a metric to measure and track the usability of your product or service based on user feedback."

[menucontent]
  section = "evaluation"

  [menu.main]
    parent = "methods"

[sidebar]
  difficulty = "Low"
  project_stage = "Evaluation"
  great_for = ["Quantifying usability", "Sharing with stakeholders", "Supporting decision-making during product development"]
  materials = ["SUS survey", "SUS calculations"]
  resources = [""]
  takes = "1 day"

[capabilities]
  user_reqs = "no"
  focus = "no"
  evaluate = "yes"
  generate = "no"
  measure_ux = "yes"
  compare = "yes"

+++

## Overview

The System Usability Scale, or SUS, is a simple survey that provides a high-level score for the usability of a product. A SUS score appeals to stakeholders because it is simple, reliable and cost-effective. Comparing SUS scores across different stages of a project shows how usability is changing.

You calculate the SUS score using the answers to a ten-item questionnaire. Users answer the questions based on their experiences with the product. The score is a value between 0 and 100, with higher values indicating better usability.

Note that:

- A SUS score is not an absolute measurement of usability so a single SUS score has limited use.
- Users express positive, neutral or negative attitudes in the SUS survey. To find out the reasons for these attitudes, you need to observe users interacting with the product. For this reason, SUS surveys are usually done with usability tests.
- You can compare SUS scores for different products. Be aware that products with comparable usability might have different SUS scores. Some causes of systematic variations could be:
 - One product being used in the laboratory but another being used in an office.
 - A product using a new process rather than a familiar one.
 - One SUS survey having a high proportion of experts in the task compared to the other surveys.

### What does SUS Consist of?

SUS comprises the following standard questions. Each has 5 answers ranging from “Strongly Agree” to “Strongly Disagree”.

 1. I think that I would like to use this system frequently.
 2. I found the system unnecessarily complex.
 3. I thought the system was easy to use.
 4. I think that I would need the support of a technical person to be able to use this system.
 5. I found the various functions in this system were well integrated.
 6. I thought there was too much inconsistency in this system.
 7. I would imagine that most people would learn to use this system very quickly.
 8. I found the system very cumbersome to use.
 9. I felt very confident using the system.
 10. I needed to learn a lot of things before I could get going with this system.

### Who’s Involved?

- Current or future users of the product
- One team member to collect responses and calculate the SUS score

## How To

### 1. Decide when to use SUS

You need to repeat the SUS survey at different points during a product lifecycle, for example:

- After every usability test, to measure the user’s perception of the product under test.
- Between development sprints, iterations or product versions.

Comparing the SUS score at each stage, shows how the product usability is progressing.

### 2. Choose the collection mechanism

The format of the survey is simple. You can collect survey responses verbally or using a printed survey. You can also use online survey tools, such as Google Forms, Survey Monkey, or TypeForm.

Consider how easy it will be to collate results. You will also want to examine individual responses, and groups of responses.

### 3. Choose the participants

Select users to complete the SUS questionnaire. Results are more statistically meaningful if you have more users. However, you can calculate a SUS value with as few as 2 users.

Users must represent the user base that you are evaluating. Consider different demographics, locations, and customer or business areas. Take into account how the results will be interpreted and applied.

### 4. Obtain responses

Getting responses can be as simple as interviewing users. You can read out each question and ask the user for a 1-5 score. Alternatively, you can send a paper or electronic survey. Encourage users to respond to questions quickly, as this tends to provide more accurate, candid results.

### 5. Calculate the SUS score

To calculate a score between 0 and 100 for the product:

1. Convert SUS responses to numbers, 1 for “Strongly Disagree”, and 5 for “Strongly Agree”.
2. For odd-numbered questions, subtract 1 from the response.
3. For even-numbered questions, subtract the response from 5.
4. Add the scores from each question and multiply the total by 2.5.
5. Remember to present the numbers as a SUS score, not a percentage.

If there are a small numbers of participants, consider calculating a confidence interval around the SUS score. This can help you understand the variability.

### Tips:

- Aim to get a cross section of participants during usability testing.
- Use different participants in each round of testing. Re-using participants can bias the SUS score.
- The questions alternate between positive and negative phrasing. Check for responses that seem to be at the wrong end of the spectrum (Strongly Disagree or Strongly Agree). These stand out from the other answers if they are a mistake. Contact the participant to confirm their intention.
- If English is not the first language of the participants, consider translating the questionnaire.

## Tips for Life Sciences

- There is some overlap with usability testing considerations. In particular, the lab environment is an important consideration for some SUS questions.
- Consider how and when you will get participants to answer the survey questions. Make sure you can observe and collect responses but not get in people’s way.
- A busy or noisy lab environment may have different outcomes to that of a calm and quiet closed meeting room.
- Consider safety implication, such as personal protective equipment. For example, people will not use a product frequently if they have to keep removing gloves.
- The workflow in a lab environment may not be the same for all participants.
- Make sure you know about any specific dangerous substances that are used in the lab.

## Resources

- [Information and Template - System Usability Scale (pdf)](http://hell.meiert.org/core/pdf/sus.pdf)
