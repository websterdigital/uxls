+++
date = "2018-01-13T00:00:00+00:00"
title = "About Scientists"
image = "about-scientists/about-scientists.jpg"
pageclass = "ux-approach-and-principles"

[menu.main]
parent = "guide-to-ux"
weight = 150
+++

## Learning From Scientists

{{<principles>}}

{{<principle title="Point Of View" image="/guide-to-ux/about-scientists/point-of-view.png">}}
Scientists strive to solve specific problems and research questions, and often have unique motivations.
{{</principle>}}

{{<principle title="Principal Investigators" image="/guide-to-ux/about-scientists/principal-investigator.png">}}
Often have a more strategic vision and can better set their science in context. May have less time in the lab.
{{</principle>}}

{{<principle title="Early Career" image="/guide-to-ux/about-scientists/early-career.png">}}
Can be very helpful and knowledgeable about what goes on in the lab. May have more time to spend discussing ideas.
{{</principle>}}

{{<principle title="Lab Technicians" image="/guide-to-ux/about-scientists/technician.png">}}
Have deep knowledge of the day-to-day operations of the lab.
{{</principle>}}

{{</principles>}}

---

## When Working With Scientists

### Prepare For Questions

You may need to prepare on the subject matter or work process in advance to better understand what you will be observing and ask relevant probing questions

### Subject Matter Experts (SMEs)

Don’t be afraid to partner with a subject matter expert if needed to prepare for and run your UX activities

### Expect Non Linear Workflows

Work in a scientific context is often complex, so be prepared to understand process scenarios, exceptions, and alternate flows

### Use Realistic Data

When possible, use realistic data in your designs or prototypes to build credibility of the solution and better engage scientists

### Respect Lab Environment and Safety

Consider your company’s policies and procedures around personal protective equipment, hazardous materials, and other considerations related to the laboratory setting
