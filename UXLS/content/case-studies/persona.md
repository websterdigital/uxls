+++
date = "2018-01-01T14:06:12+02:00"
author = "Europe PMC"
company = "Europe PMC"
featured = false
title = "Europe PMC personas and scenarios"
methods = ["personas.md"]
summary = "Learn how Europe PMC used personas to learn more about scientists' literature search behaviour, and understand how to build trust in key user communities."
image = "europepmc-feedback-analysis.jpg"
companylogo = "project-logos/europepmc-logo.png"

[menu.main]
  parent = "case-studies"

[menucontent]
  image = "europepmc-feedback-analysis.jpg"

[[sidebar]]
    title = "Team"
    content = ["2 UX researchers", "16 life scientists", "2 curators"]

[[sidebar]]
    title = "Timeline"
    content = ["4 months"]

[[sidebar]]
    title = "Deliverables"
    content = ["Research report", "Experience map", "Personas", "Scenarios"]
+++

{{< section sidebar="true" style="content" >}}

## Europe PMC personas and scenarios

[Europe PMC](http://www.europepmc.org/) is a literature search resource for life scientists. The Europe PMC UX team wanted to learn more about scientists' literature search behaviour, and understand how to build trust in key user communities, to inform a redesign of Europe PMC features and functionality. The goals of the redesign project are to attract new users, increase views of article full text and encourage deeper engagement with the content. The user research included a diary study and usability study.
{{< /section >}}

{{< section >}}
## Process

### Plan

Europe PMC is a repository of life sciences literature. UX researchers carried out a qualitative study to learn about the literature search behavior of life scientists. The methodology included a diary study, where participants provided information about their literature searches over one week, and usability testing to compare literature searches on PubMed and Europe PMC. User personas and search scenarios were created to summarise and communicate insights about user behavior to developers and other stakeholders.

### Conduct

We asked 12 participants to document their literature searches during one working week, using search tools they were familiar with. We interviewed and briefed participants before the study began. Participants documented their search activity by answering a simple set of questions about each search. These updates were sent daily. We conducted a de-briefing interview at the end of the week to discuss the participant’s literature searches. The interviews were recorded.

Next, we carried out usability testing with 6 more participants. We asked them about their literature searching behavior and recent literature searches. After that we asked them to complete tasks related to recent literature searches using the search tool they use most frequently, and then they repeated the same tasks on Europe PMC.

### Analyze and Report

Interview notes and the updates provided by participants were printed and cut into excerpts, which were then sorted into groups with common themes. The lead researcher created an [experience map](https://doi.org/10.6084/m9.figshare.4789738.v1) to summarise user behavior and the stages of a search. The researcher developed 4 personas to illustrate similar search behaviors in different types of users. Based on the personas, the researcher then developed user scenarios to provide more context and detail about search behaviors and user needs.
{{< /section >}}


{{< section style="is-flex" small="4" >}}
{{% img src="europepmc-notes.jpg" caption="Figure 1: A participant update." %}}
{{% img src="europepmc-feedback-analysis.jpg" caption="Figure 2: Participant data was affinity sorted into themes. The small sticky notes identified what participants were doing (green), thinking (yellow) and feeling (pink)." %}}
{{% img src="europepmc-experience-map.png" caption="Figure 3: The search experience map provided a summary of user behavior and needs." %}}
{{< /section >}}

{{< section style="is-flex" small="6" >}}
{{% img src="europepmc-persona.jpg" caption="Figure 4: An example user persona." %}}
{{% img src="europepmc-scenario.jpg" caption="Figure 5: A literature search scenario, based on a particular persona." %}}
{{< /section >}}

{{< section >}}
Europepmc Persona and Scenario image attribution: “[Woman](https://www.flickr.com/photos/gldar/33088186182/)” by [G Darrud](https://www.flickr.com/photos/gldar/) licensed as [CC BY-NC 2.0](https://creativecommons.org/licenses/by-nc/2.0/).
{{< /section >}}

{{< section style="content" >}}
## Outcome

The UX researchers identified 5 common different types of literature search including:
- exploring a topic
- finding recent articles on a familiar topic
- finding evidence in articles
- learning about a methodology
- finding a specific article


The researchers also found that searches involve different stages (see the [search experience map](https://doi.org/10.6084/m9.figshare.4789738.v1)). Literature search behaviors vary depending on the type of search and stage of the search.

The process of creating the personas and scenarios helped to summarise the different types of user search behavior. The search scenarios describe the behaviors and user needs in a way that the wider development team can relate to and empathise with. The insights from this research have informed a redesign of key pages and search functionality on the Europe PMC website. The search scenarios will be used by the team to check that user needs and behaviors are supported, in addition to testing website prototypes with users.
{{< /section >}}
