+++
date = "2017-06-01T13:01:26+02:00"
title = "FAQs"
weight = 200

[menu.main]
parent = "about"
+++

## What is the User Experience for Life Sciences (UXLS) Project?

We are a community of practitioners from the pharmaceutical, biotechnology, and software industries. As members of the Pistoia Alliance we have created the User Experience (UX) for Life Sciences toolkit to enable business to adopt UX principles and methods as they develop scientific software. Our goal is to drive the adoption of UX in the life sciences.


## Who is involved?

There are over 15 organizations from the biopharmaceutical, agri-food and technology sector actively contributing to the development of this project. With over 50 UX specialists we are harnessing our shared knowledge for the benefit of the life science community.


## What is the Pistoia Alliance?

The Pistoia Alliance is a global, not-for-profit alliance of life science companies, vendors, publishers, and academic groups that work together to lower barriers to innovation in R&D. We develop best practices and technology pilots to overcome common obstacles. Our members collaborate as equals on open projects that generate significant value for the worldwide life sciences community.


## Are there any restrictions to using the toolkit?

The toolkit is free to use, subject to the terms of our [Terms of Use](../../terms-of-use). Among other things, we require appropriate attribution and impose certain restrictions on the copying of case studies and any associated company or Pistoia Alliance branding (such as company logos). Refer to our [Terms of Use](../../terms-of-use) for details.

## What’s next?

We will be expanding this guide over the coming months with more UX methods, metrics and case studies.


## How do I get involved?

We are always looking for project contributions and feedback. Please get in touch with our project manager, Paula de Matos ([paula.dematos@pistoiaalliance.org](mailto:paula.dematos@pistoiaalliance.org)).
