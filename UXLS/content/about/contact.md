+++
date = "2017-06-01T13:01:46+02:00"
title = "Contact Us"
weight = 300

[menu.main]
parent = "about"
+++

### Do you have a question or would you like to get involved?

We are always looking for project contributions and feedback. Please get in touch with our project manager, Paula de Matos ([paula.dematos@pistoiaalliance.org](mailto:paula.dematos@pistoiaalliance.org)).

### Legal inquiries

Martyn Wilkins, Secretary\
Pistoia Alliance, Inc. c/o Virtual, Inc.\
401 Edgewater Place, Suite 600\
Wakefield MA 01880, USA

Fax: +1 (781) 224 1239\
Email: martyn.wilkins@pistoiaalliance.org
