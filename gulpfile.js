'use strict';

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require ('gulp-concat'),
    ugilfy = require ('gulp-uglify'),
    cssnano = require ('gulp-cssnano'),
    autoprefixer = require ('gulp-autoprefixer'),
    notify = require('gulp-notify'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    del = require('del');

var basedir = process.env.GULP_BASEDIR + "/"

var source = basedir + 'UXLS/themes/pa-uxls/src/',
    dest = basedir + 'UXLS/themes/pa-uxls/static/static/';

var bootstrap = { in: 'node_modules/bootstrap-sass/' };

var scss = {
    in: source + 'styles.scss',
    out: dest,
    watch: source + '**/*.scss',
    sassOpts: {
        outputStyle: 'nested',
        precison: 3,
        errLogToConsole: true,
        includePaths: [bootstrap.in + 'assets/stylesheets']
        }
    };

var fonts = {
        in: [source + 'fonts/*.*',
            bootstrap.in + 'assets/fonts/bootstrap/**/*'],
        out: dest + 'fonts/'
    };

var img = {
        in: [source + 'img/*.*'],
        out: dest + 'img/'
    };

var scripts = {
    in: [source + '/**/*.js',
        bootstrap.in + 'assets/javascripts/bootstrap.js'], // Add additional script sources in bower_components here
    out: dest
    };

gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

gulp.task('img', function() {
    return gulp.src(img.in)
        .pipe(gulp.dest(img.out));
});

gulp.task('scripts', function() {
    return gulp.src(scripts.in)
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.js'))
        .pipe(ugilfy())
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(scripts.out));
});

gulp.task('sass', function () {
    return gulp.src(scss.in)
        .pipe(sourcemaps.init())
        .pipe(sass(scss.sassOpts)
        .on('error', sass.logError))
        .once("error", function () { this.once("finish", () => process.exit(1)); })
        .pipe(autoprefixer({
            browsers: ['last 2 versions'], // TBD: minimum compatability
            cascade: false
        }))
        .pipe(cssnano())
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(scss.out));
});

gulp.task('cleanup', function(){
    return del([dest]);
});

gulp.task('watch', function() {
    gulp.watch(scss.watch, ['sass']);
    gulp.watch(source + '/**/*.js', ['scripts']);
});

gulp.task('default', ['cleanup'], function () {
    gulp.start(
        'sass',
        'scripts',
        'fonts',
        'img'
    );
});
